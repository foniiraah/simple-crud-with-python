import mysql.connector
import os

mydb = mysql.connector.connect(
    host = "localhost",
    username = "root",
    password = "",
    database = "pegawai"
)

def insert_data(mydb):
    # id = input ("Masukan id : ")
    nama = input ("Masukan Nama : ")
    jabatan = input ("Masukan Jabatan : ")
    alamat = input ("Masukan Alamat : ")
    val = (nama, jabatan, alamat)
    cursor = mydb.cursor()
    sql = "INSERT INTO tblpegawai (nama, jabatan, alamat) VALUES (%s, %s, %s)"
    cursor.execute(sql, val)
    mydb.commit()
    print("{} data berhasil disimpan".format(cursor.rowcount))

def show_data(mydb):
    cursor = mydb.cursor()
    sql = "SELECT * FROM tblpegawai"
    cursor.execute(sql)
    results = cursor.fetchall()

    if cursor.rowcount < 0:
        print("Tidak ada Data")
    else:
        for data in results:
            print(data)

def update_data(mydb):
    cursor = mydb.cursor()
    show_data(mydb)

    id = input("pilih id pegawai > ")
    nama = input ("Masukan Nama : ")
    jabatan = input ("Masukan Jabatan : ")
    alamat = input ("Masukan Alamat : ")
    sql = "UPDATE tblpegawai SET nama=%s , jabatan=%s, alamat=%s WHERE id=%s"
    val = (nama, jabatan, alamat, id)
    cursor.execute(val, sql)
    mydb.commit()
    print("{} data berhasil di update".format(cursor.rowcount))

def delete_data(mydb):
    cursor = mydb.cursor()
    show_data(mydb)

    id = input("pilih id pegawai > ")
    sql = "DELETE FROM tblpegawai WHERE id=%s"
    val = (id,)
    cursor.execute(val, sql)
    mydb.commit()
    print("{} data berhasil di hapus".format(cursor.rowcount))

def search_data(mydb):
    cursor = mydb.cursor()
    keyword = input("Masukan kata kunci : ")
    sql = "SELECT * FROM tblpegawai nama LIKE %s OR alamat LIKE %s"
    val = ("%{}%".format(keyword), "%{}%".format(keyword))
    cursor.execute(val, sql)
    results = cursor.fetchall()

    if cursor.rowcount < 0:
        print ("Tidak Ada Data")
    else:
        for data in results:
            print (data)
    
def show_menu(mydb):
    print("--- Aplikasi Database Python ---")
    print("1. Insert Data")
    print("2. Tampilkan Data")
    print("3. Update Data")
    print("4. Delete Data")
    print("5. Cari Data")
    print("0. Keluar")
    print("---------------------")
    menu = input("Pilih menu> ")

  #clear screen
    os.system("clear")

    if menu == "1":
        insert_data(mydb)
    elif menu == "2":
        show_data(mydb)
    elif menu == "3":
        update_data(mydb)
    elif menu == "4":
        delete_data(mydb)
    elif menu == "5":
        search_data(mydb)
    elif menu == "0":
        exit()
    else:
        print("Menu salah!")


if __name__ == "__main__":
  while(True):
    show_menu(mydb)




